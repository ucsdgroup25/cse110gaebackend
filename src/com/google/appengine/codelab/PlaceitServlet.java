/**
 * Copyright 2011 Google
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.appengine.codelab;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.codelab.Util;

/**
 * This servlet responds to the request corresponding to Placeit entities. The servlet
 * manages the Placeit Entity
 * 
 * 
 */
@SuppressWarnings("serial")
public class PlaceitServlet extends BaseServlet {

  private static final Logger logger = Logger.getLogger(PlaceitServlet.class.getCanonicalName());
  /**
   * Get the entities in JSON format.
   */

  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
	super.doGet(req, resp);
	
    logger.log(Level.INFO, "Obtaining Placeit listing");
    String searchFor = req.getParameter("q");
    PrintWriter out = resp.getWriter();
    Iterable<Entity> entities = null;
    
    //Create Placeit on GAE
    if (searchFor == null || searchFor.equals("") || searchFor == "*") {
      entities = Placeit.getAllPlaceit();
      out.println(Util.writeJSON(entities));
    }
    //Edit Placeit on GAE
    else {
      Entity placeit = Placeit.getSinglePlaceit(searchFor);
      if (placeit != null) {
        Set<Entity> result = new HashSet<Entity>();
        result.add(placeit);
        out.println(Util.writeJSON(result));
      }
    }
  }

  /**
   * Create the entity and persist it.
   */
  protected void doPut(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    logger.log(Level.INFO, "Creating Product");
    PrintWriter out = resp.getWriter();

    //Extract all placeit properties
    String name = req.getParameter("name");
    String description = req.getParameter("description");
    String latitude = req.getParameter("latitude");
    String longitude = req.getParameter("longitude");
    String create_time = req.getParameter("create_time");
    String due_flag = req.getParameter("due_flag");
    String rep_date = req.getParameter("rep_date");
    String cat1 = req.getParameter("cat1");
    String cat2 = req.getParameter("cat2");
    String cat3 = req.getParameter("cat3");
    
    try {
      Placeit.createOrUpdatePlaceit(name, description, latitude, longitude, create_time, due_flag, rep_date, cat1, cat2, cat3);
    } catch (Exception e) {
      String msg = Util.getErrorMessage(e);
      out.print(msg);
    }
  }

  /**
   * Delete the placeit entity
   */
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
	//Get the key from the http request
    String placeitkey = req.getParameter("id");
    
    PrintWriter out = resp.getWriter();
    try{    	
    	out.println(Placeit.deletePlaceit(placeitkey));
    } catch(Exception e) {
    	out.println(Util.getErrorMessage(e));
    }    
  }

  /**
   * Redirect the call to doDelete or doPut method
   */
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String action = req.getParameter("action");
    
    if (action.equalsIgnoreCase("delete")) {
      doDelete(req, resp);
      return;
    } else if (action.equalsIgnoreCase("put")) {
      doPut(req, resp);
      return;
    }
  }

}