package com.google.appengine.codelab;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;

import java.util.List;



/**
 * Handles managing data related to placeits for the google app engine
 * 
 */
public class Placeit 
{
	/**
	   * Update the placeit
	   * @param name: name of the placeit
	   * @param description : description
	   * @param lat : latitude
	   * @param longi : longitude
	   * @param create_time : Time of placeit creation
	   * @param due_flag : flag indicating status of due date
	   * @param rep_date : day of week placeit should be repeated
	   * @param cat1 : category 1
	   * @param cat2 : category 2
	   * @param cat3 : category 3
	   * @return  updated placeit
	   */
	  public static void createOrUpdatePlaceit(String name, String description, String lat, String longi, String create_time, String due_flag, String rep_date, String cat1, String cat2, String cat3) {
	    Util.listEntities("PlaceIt", "lat", lat);
		  
		Entity placeit = getSinglePlaceit(lat+longi);
	  	if (placeit == null) 
	  	{
	  		placeit = new Entity("PlaceIt", (lat+longi));
	  		placeit.setProperty("name", name);
	  		placeit.setProperty("description", description);
	  		placeit.setProperty("lat", lat);
	  		placeit.setProperty("long", longi);
	  		placeit.setProperty("create_time", create_time);
	  		placeit.setProperty("due_flag", due_flag);
	  		placeit.setProperty("rep_date", rep_date);
	  		placeit.setProperty("cat1", cat1);
	  		placeit.setProperty("cat2", cat2);
	  		placeit.setProperty("cat3", cat3);
	  	} 
	  	else 
	  	{
	  		placeit.setProperty("due_flag", due_flag);
	  	}
	  	Util.persistEntity(placeit);

	  }
	
	 /**
	   * Update the placeit
	   * @param name: name of the product
	   * @param description : description
	   * @param lat : latitude
	   * @param longi : longitude
	   * @return  updated product
	   */
	  public static void createOrUpdatePlaceit(String name, String description, String lat, String longi) {
	    
		//Get placeit
		//Placeits are indexed based on lat and longitude
		Entity placeit = getSinglePlaceit(lat+longi);
	    
	  	if (placeit == null) {
	  		placeit = new Entity("PlaceIt", (lat+longi));
	  		placeit.setProperty("name", name);
	  		placeit.setProperty("description", description);
	  		placeit.setProperty("lat", lat);
	  		placeit.setProperty("long", longi);
	  		placeit.setProperty("create_time", "");
	  		placeit.setProperty("due_flag", "");
	  		placeit.setProperty("rep_date", "");
	  		placeit.setProperty("cat1", "");
	  		placeit.setProperty("cat2", "");
	  		placeit.setProperty("cat3", "");
	  	} else {
	  		placeit.setProperty("name", name);
	  		placeit.setProperty("description", description);
	  		placeit.setProperty("lat", lat);
	  		placeit.setProperty("long", longi);
	  		placeit.setProperty("create_time", "");
	  		placeit.setProperty("due_flag", "");
	  		placeit.setProperty("rep_date", "");
	  		placeit.setProperty("cat1", "");
	  		placeit.setProperty("cat2", "");
	  		placeit.setProperty("cat3", "");
	  	}
	  	Util.persistEntity(placeit);
	  }

	  /**
	   * Retrun all the placeits
	   * @return  list of all placeits
	   */
	  public static Iterable<Entity> getAllPlaceit() {
		  	Iterable<Entity> entities = Util.listEntities("PlaceIt", null, null);
		  	return entities;
	  }

	  /**
	   * Get product entity
	   * @param name : name of the product
	   * @return: product entity
	   */
	  public static Entity getSinglePlaceit(String name) {
	  	Key key = KeyFactory.createKey("PlaceIt",name);
	  	return Util.findEntity(key);
	  }
	  
	  /**
	   * Get all placeits by  id
	   * @param name: id of the placeit based on latitude and longitude
	   * @return list of items
	   */
	  
	  public static List<Entity> getPlaceit(String id) {
	  	Query query = new Query();
	  	Key parentKey = KeyFactory.createKey("PlaceIt", id);
	  	query.setAncestor(parentKey);
	  	query.addFilter(Entity.KEY_RESERVED_PROPERTY, Query.FilterOperator.GREATER_THAN, parentKey);
	  		List<Entity> results = Util.getDatastoreServiceInstance()
	  				.prepare(query).asList(FetchOptions.Builder.withDefaults());
	  		return results;
		}
	  
	  /**
	   * Delete placeit entity
	   * @param placeitKey: placeit to be deleted
	   * @return status string
	   */
	  public static String deletePlaceit(String placeitKey)
	  {
		  Key key = KeyFactory.createKey("PlaceIt",placeitKey);	   
		  
		  List<Entity> placeits = getPlaceit(placeitKey);	  
		  if (!placeits.isEmpty()){
		      return "Cannot delete, as there are items associated with this product.";	      
		    }	    
		  Util.deleteEntity(key);
		  return "Product deleted successfully";
		  
	  }

}
